"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const indexController_1 = require("../controllers/indexController");
class IndexRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {

        //EJEMPOOOOOOOOOOOOOO=======================================
        //get
        //this.router.get('/', indexController_1.indexController.getUsers);
        //this.router.get('/:id', indexController_1.indexController.getUser);
        //post
        //this.router.post('/user', indexController_1.indexController.createUser);
        //put
        //this.router.put('/user/:id', indexController_1.indexController.updateUser);
        //delete
        //this.router.delete('/user/:id', indexController_1.indexController.deleteUser);



        //PROYECTO ANALISIS
        //post CREAR UN USUARIO
        this.router.post('/analisis/user', indexController_1.indexController.createUserAnalisis);

        //get OBTENER TODOS LOS USUARIOS //SI
        this.router.get('/analisis/', indexController_1.indexController.getUsersAnalisis);

        //get LOGIN
        this.router.get('/analisis/login', indexController_1.indexController.getUserAnalisisLogin);

        //AGREGAR PELICULAS SI
        this.router.post('/analisis/newpelicula', indexController_1.indexController.createPeliculaAnalisis);
        
        //VER PELICULAS SI
        this.router.post('/analisis/verpeliculas', indexController_1.indexController.getPeliculas);
        
        //GET USUARIO LOGEADO
        this.router.post('/analisis/login2', indexController_1.indexController.getLogin);
        
        //AGREGAR LENGUAJE SI
        this.router.post('/analisis/newlenguage', indexController_1.indexController.createLenguaje);
      
        //VER UNA PELICULA
        this.router.post('/analisis/verpelicula/ver', indexController_1.indexController.getPelicula);

        //VER LENGUAJE DE PELICULA
        this.router.post('/analisis/verpelicula/leng', indexController_1.indexController.getPeliculalenguajes);
        
        //VER UN USUARIO
        this.router.post('/user/user/user', indexController_1.indexController.getUser);

        //ver disponibilidad de pelicula
        this.router.post('/analisis/verpelicula/disp', indexController_1.indexController.getAvailability);
    
        //ver carrito
        this.router.post('/analisis/carrito/ver', indexController_1.indexController.getCarrito);
        
        //post CREAR UN carrito
        this.router.post('/analisis/carrito/crear', indexController_1.indexController.createCarrito);

        //ver alquiler
        this.router.post('/analisis/alquiler/ver', indexController_1.indexController.getAlquiler);
        
        //post CREAR UN alquiler
        this.router.post('/analisis/alquiler/crear', indexController_1.indexController.createAlquiler);

        //post rentar una peli
        this.router.post('/analisis/alquiler/rentar', indexController_1.indexController.updatePeliculaFalse);

        //post devolver una peli
        this.router.post('/analisis/alquiler/devolver', indexController_1.indexController.updatePeliculaTrue);

        //post eliminar del carrito
        this.router.post('/analisis/carrito/eliminar', indexController_1.indexController.deletecarrito);

        //VER correo de usuario
        this.router.post('/user/user/user1', indexController_1.indexController.getUser1);
        //VER correo de usuario
        this.router.post('/analisis/transaccion/porusuario', indexController_1.indexController.getTransaccionUsuario);
        //VER correo de usuario
        this.router.post('/analisis/transaccion/ver', indexController_1.indexController.getTransaccion);

        //crear transaccion
        this.router.post('/analisis/transaccion/crear', indexController_1.indexController.createTransaccion);
    }
}
const indexRoutes = new IndexRoutes();
exports.default = indexRoutes.router;
